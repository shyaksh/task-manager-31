package ru.bokhan.tm.exception.incorrect;

public final class IncorrectDataFileException extends RuntimeException {

    public IncorrectDataFileException() {
        super("Error! Incorrect data file...");
    }

}
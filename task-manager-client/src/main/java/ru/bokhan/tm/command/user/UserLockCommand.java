package ru.bokhan.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bokhan.tm.api.ICurrentSessionService;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.endpoint.SessionDTO;
import ru.bokhan.tm.endpoint.UserEndpoint;
import ru.bokhan.tm.exception.security.AccessDeniedException;
import ru.bokhan.tm.util.TerminalUtil;

@Component
public final class UserLockCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private ICurrentSessionService currentSessionService;

    @NotNull
    @Override
    public String name() {
        return "user-lock";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Lock user by login.";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = currentSessionService.getCurrentSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN");
        @NotNull final String login = TerminalUtil.nextLine();
        userEndpoint.lockUserByLogin(session, login);
        System.out.println("[OK]");
    }

}

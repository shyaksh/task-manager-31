package ru.bokhan.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.bokhan.tm.command.AbstractCommand;

@Component
public final class VersionCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @NotNull
    @Override
    public String argument() {
        return "-v";
    }

    @NotNull
    @Override
    public String description() {
        return "Show version info.";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

}

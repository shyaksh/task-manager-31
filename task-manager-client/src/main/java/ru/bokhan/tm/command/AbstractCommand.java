package ru.bokhan.tm.command;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.lang.Nullable;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractCommand {

    @NotNull
    public abstract String name();

    @Nullable
    public abstract String argument();

    @Nullable
    public abstract String description();

    public abstract void execute();

}

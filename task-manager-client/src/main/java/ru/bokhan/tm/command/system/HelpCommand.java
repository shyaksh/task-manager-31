package ru.bokhan.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bokhan.tm.command.AbstractCommand;

@Component
public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private AbstractCommand[] abstractCommands;

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @NotNull
    @Override
    public String argument() {
        return "-h";
    }

    @NotNull
    @Override
    public String description() {
        return "Display terminal commands.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        for (AbstractCommand command : abstractCommands) {
            System.out.printf("%-25s| %s\n", command.name(), command.description());
        }
        System.out.println("[OK]");
    }

}

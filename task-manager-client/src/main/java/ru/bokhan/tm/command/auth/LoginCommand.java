package ru.bokhan.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bokhan.tm.api.ICurrentSessionService;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.endpoint.SessionDTO;
import ru.bokhan.tm.endpoint.SessionEndpoint;
import ru.bokhan.tm.exception.security.AccessDeniedException;
import ru.bokhan.tm.util.TerminalUtil;

@Component
public final class LoginCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @NotNull
    @Autowired
    private ICurrentSessionService currentSessionService;

    @NotNull
    @Override
    public String name() {
        return "login";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Log in.";
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        System.out.println("[ENTER LOGIN:]");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD:]");
        @NotNull final String password = TerminalUtil.nextLine();
        @Nullable final SessionDTO session = sessionEndpoint.openSession(login, password);
        if (session == null) throw new AccessDeniedException();
        currentSessionService.setCurrentSession(session);
        System.out.println("[OK:]");
    }

}

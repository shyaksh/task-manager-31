package ru.bokhan.tm.command.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bokhan.tm.api.ICurrentSessionService;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.endpoint.DomainEndpoint;
import ru.bokhan.tm.endpoint.SessionDTO;
import ru.bokhan.tm.exception.security.AccessDeniedException;

@Component
public final class DataBase64LoadCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private DomainEndpoint domainEndpoint;

    @NotNull
    @Autowired
    private ICurrentSessionService currentSessionService;

    @NotNull
    @Override
    public String name() {
        return "data-base64-load";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from base64 binary file.";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = currentSessionService.getCurrentSession();
        if (session == null) throw new AccessDeniedException();
        if (domainEndpoint.loadFromBase64(session)) System.out.println("[OK]");
    }

}

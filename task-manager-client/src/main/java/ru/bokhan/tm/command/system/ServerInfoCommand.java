package ru.bokhan.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.endpoint.ServerInfoEndpoint;

@Component
public final class ServerInfoCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private ServerInfoEndpoint serverInfoEndpoint;

    @NotNull
    @Override
    public String name() {
        return "server-info";
    }

    @NotNull
    @Override
    public String argument() {
        return "-s";
    }

    @NotNull
    @Override
    public String description() {
        return "Show server host and port.";
    }

    @Override
    public void execute() {
        System.out.println("[SERVER INFO]");
        System.out.println("Server host: " + serverInfoEndpoint.getServerHost());
        System.out.println("Server port: " + serverInfoEndpoint.getServerPort());
    }

}

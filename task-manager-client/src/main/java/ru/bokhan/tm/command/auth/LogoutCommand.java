package ru.bokhan.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bokhan.tm.api.ICurrentSessionService;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.endpoint.SessionDTO;
import ru.bokhan.tm.endpoint.SessionEndpoint;
import ru.bokhan.tm.exception.security.AccessDeniedException;

@Component
public final class LogoutCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @NotNull
    @Autowired
    private ICurrentSessionService currentSessionService;

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Log out.";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        @Nullable SessionDTO session = currentSessionService.getCurrentSession();
        if (session == null) throw new AccessDeniedException();
        sessionEndpoint.closeSession(session);
        currentSessionService.setCurrentSession(null);
        System.out.println("[OK:]");
    }

}

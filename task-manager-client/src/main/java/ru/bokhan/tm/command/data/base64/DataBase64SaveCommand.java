package ru.bokhan.tm.command.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bokhan.tm.api.ICurrentSessionService;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.endpoint.DomainEndpoint;
import ru.bokhan.tm.endpoint.SessionDTO;
import ru.bokhan.tm.exception.security.AccessDeniedException;

@Component
public final class DataBase64SaveCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private DomainEndpoint domainEndpoint;

    @NotNull
    @Autowired
    private ICurrentSessionService currentSessionService;

    @NotNull
    @Override
    public String name() {
        return "data-base64-save";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to base64 binary file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA BASE64 SAVE]");
        @Nullable final SessionDTO session = currentSessionService.getCurrentSession();
        if (session == null) throw new AccessDeniedException();
        if (domainEndpoint.saveToBase64(session)) System.out.println("[OK]");
    }

}

package ru.bokhan.tm.bootstrap;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.exception.system.UnknownCommandException;
import ru.bokhan.tm.util.TerminalUtil;

import java.util.LinkedHashMap;
import java.util.Map;

@Getter
@Setter
@Component
public final class Bootstrap {

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Autowired
    @Nullable private AbstractCommand[] abstractCommands;

    private void initCommands(@Nullable final AbstractCommand[] commands) {
        if (commands == null) return;
        for (AbstractCommand command : commands) {
            if (command == null) continue;
            this.commands.put(command.name(), command);
        }
    }

    public void run(@Nullable final String[] arguments) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        initCommands(abstractCommands);
        if (parseArguments(arguments)) System.exit(0);
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                logError(e);
            }
        }
    }

    private void logError(@NotNull final Exception e) {
        System.err.println(e.getMessage());
        System.err.println("[FAIL]");
    }

    public void parseArgument(@Nullable final String argument) {
        if (argument == null || argument.isEmpty()) return;
        @Nullable final AbstractCommand command = getCommandByArgument(argument);
        if (command == null) return;
        command.execute();
    }

    public void parseCommand(@Nullable final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        @Nullable final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        command.execute();
    }

    public boolean parseArguments(@Nullable final String[] arguments) {
        if (arguments == null || arguments.length == 0) return false;
        @Nullable final String argument = arguments[0];
        parseArgument(argument);
        return true;
    }

    @Nullable
    private AbstractCommand getCommandByArgument(@Nullable final String argument) {
        if ((argument == null) || argument.isEmpty()) return null;
        for (@NotNull final AbstractCommand command : commands.values()) {
            if (argument.equals(command.argument())) return command;
        }
        return null;
    }

}